package de.irame.thelist.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Felix on 21.06.2014.
 *
 * Singleton to handle the db connection and initialise it.
 */
public class DBWrapper {
    // Singleton signature START
    private static DBWrapper ourInstance = new DBWrapper();
    private Connection connection;

    private DBWrapper() {

        //connecting to the database
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:TheListSaveData.db");

            Statement statement = connection.createStatement();

            //enable foreign key support
            statement.execute("PRAGMA foreign_keys = ON");

            statement.execute("" +
                    "CREATE TABLE IF NOT EXISTS champion_lock (" +
                    "champ_ID INTEGER PRIMARY KEY," +
                    "champ_is_locked INTEGER," +
                    "champ_locked_since INTEGER" +
                    ")");

            statement.execute("" +
                    "CREATE TABLE IF NOT EXISTS options (" +
                    "opt_key VARCHAR(50) PRIMARY KEY," +
                    "opt_value TEXT" +
                    ")");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static DBWrapper getInstance() {
        return ourInstance;
    }
    // Singleton signature END

    /**
     * Returns the connection to the database
     *
     * @return Connection to the database
     */
    public Connection getConnection() {
        return connection;
    }
}
