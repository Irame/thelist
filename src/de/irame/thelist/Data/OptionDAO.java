package de.irame.thelist.Data;

/**
 * Created by Felix on 23.06.2014.
 */
public interface OptionDAO {
    /**
     * Gets an boolean option for an specific key. If there is no value for this key stored the default value is returned.
     *
     * @param key          key to look for
     * @param defaultValue default value which is returned if nothing was found.
     * @return the boolean option
     */
    public boolean getBoolean(String key, boolean defaultValue);

    /**
     * Gets an string option for an specific key. If there is no value for this key stored the default value is returned.
     *
     * @param key          key to look for
     * @param defaultValue default value which is returned if nothing was found.
     * @return the string option
     */
    public String getString(String key, String defaultValue);

    /**
     * Gets an int option for an specific key. If there is no value for this key stored the default value is returned.
     *
     * @param key          key to look for
     * @param defaultValue default value which is returned if nothing was found.
     * @return the int option
     */
    public int getInt(String key, int defaultValue);

    /**
     * Sets an boolean option for an specific key.
     *
     * @param key   key of the boolean option
     * @param value value to store
     */
    public void storeBoolean(String key, boolean value);

    /**
     * Sets an string option for an specific key.
     *
     * @param key   key of the string option
     * @param value value to store
     */
    public void storeString(String key, String value);

    /**
     * Sets an int option for an specific key.
     *
     * @param key   key of the int option
     * @param value value to store
     */
    public void storeInt(String key, int value);
}
