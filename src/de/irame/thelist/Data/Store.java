package de.irame.thelist.Data;

import de.irame.thelist.Debug;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Felix on 11.06.2014.
 */
@Deprecated
public class Store {
    public static final String DEFAULT_FILE = "TheListSaveData";

    public static void saveChamps(List<Champion> championList, String fileName) {
        File file = new File(fileName);
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file))) {
            for (Champion champion : championList) {
                dataOutputStream.writeInt(champion.getId());
            }
            dataOutputStream.flush();
        } catch (IOException e) {
            Debug.out(System.err::println, "Failed to write to File + " + file.getAbsolutePath());
        }
    }

    public static void saveChamps(List<Champion> championList) {
        saveChamps(championList, DEFAULT_FILE);
    }

    public static ArrayList<Integer> loadChamps(String fileName) {
        File file = new File(fileName);
        ArrayList<Integer> loadedChamps = new ArrayList<>();
        if (!file.exists()) {
            Debug.out(System.err::println, "File does not exist: " + file.getAbsolutePath());
            return loadedChamps;
        }

        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file))) {
            boolean reachedEOF = false;
            while (!reachedEOF) {
                try {
                    loadedChamps.add(dataInputStream.readInt());
                } catch (EOFException e) {
                    reachedEOF = true;
                    Debug.println("reached end of file: " + file.getAbsolutePath());
                }
            }
        } catch (IOException e) {
            Debug.out(System.err::println, "Failed to read from File " + file.getAbsolutePath());
        }

        return loadedChamps;
    }

    public static ArrayList<Integer> loadChamps() {
        return loadChamps(DEFAULT_FILE);
    }
}
