package de.irame.thelist.Data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.irame.thelist.Debug;
import de.irame.thelist.Util;
import javafx.scene.paint.Color;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Felix on 11.06.2014.
 */

/**
 * Abstract class for accessing the RiotAPI
 */
public abstract class RiotAPI {
    public static final String API_KEY = "970a9197-ce71-4c3c-a413-e09072d0d84d";

    /**
     * Gets all champs from the RiotAPI and put it into a map.
     *
     * @param championMap map to put the champions in.
     */
    public static void getAllChamps(Map<Integer, Champion> championMap) {
        try {
            Debug.startTimer("get the main JSONObject");
            //get the main JSONObject
            HttpResponse<JsonNode> response = Unirest.get("https://global.api.pvp.net/api/lol/static-data/euw/v1.2/champion?champData=image,tags&api_key=" + API_KEY).asJson();
            JSONObject jsonObject = response.getBody().getObject();
            Debug.timerDiff("get the main JSONObject");

            // get the ddragon version
            String version = jsonObject.getString("version");

            // get the champ data
            JSONObject jsonChampList = jsonObject.getJSONObject("data");
            JSONObject jsonChamp;
            Champion curChamp;
            for (String key : jsonChampList.keySet()) {
                jsonChamp = jsonChampList.getJSONObject(key);
                curChamp = getChampByJson(jsonChamp, version);
                championMap.put(curChamp.getId(), curChamp);
            }
        } catch (JSONException | UnirestException e) {
            Util.setStatusMsgLabelText("Could not load champs from RIOT API", Color.RED);
        }
    }

    /**
     * Gets all champs from the RiotAPI.
     * @return A map of all champs indexed by there ID.
     */
    public static HashMap<Integer, Champion> getAllChamps() {
        HashMap<Integer, Champion> champions = new HashMap<>();
        getAllChamps(champions);
        return champions;
    }

    /**
     * Gets a champion by ID from the RiotAPI.
     * @return A single Champion.
     */
    public static Champion getChampById(int id) {
        Champion champion = null;

        try {
            HttpResponse<JsonNode> versionResponse = Unirest.get("https://global.api.pvp.net/api/lol/static-data/euw/v1.2/versions?api_key=" + API_KEY).asJson();
            String version = versionResponse.getBody().getArray().getString(1);

            HttpResponse<JsonNode> champResponse = Unirest.get("https://global.api.pvp.net/api/lol/static-data/euw/v1.2/champion/" + id + "?version=" + version + "&champData=image,tags&api_key=" + API_KEY).asJson();
            champion = getChampByJson(champResponse.getBody().getObject(), version);
        } catch (UnirestException e) {
            Debug.out(System.err::println, "Could not load champ with id = " + id + " from RIOT API");
        }

        return champion;
    }

    // extracted method to gen an champion out of an JSONObject
    private static Champion getChampByJson(JSONObject champJson, String version) {
        JSONArray champTags = champJson.getJSONArray("tags");

        Champion champion = new Champion(
                champJson.getInt("id"),
                champJson.getString("name"),
                champJson.getString("key"),
                champJson.getJSONObject("image").getString("full"),
                version);

        for (int i = 0; i < champTags.length(); i++) {
            champion.addTag(champTags.getString(i).toLowerCase());
        }

        return champion;
    }

    /**
     * Checks if an Summoner Exists
     *
     * @param name   Name of the Summoner
     * @param region Region  to look in
     * @return true if he exists false if not
     */
    public static boolean doesSummonerExist(String name, String region) {
        try {
            int code = Unirest.get("https://euw.api.pvp.net/api/lol/" + region.toLowerCase() + "/v1.4/summoner/by-name/" + name + "?api_key=" + API_KEY).asString().getCode();
            if (404 != code)
                return true;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return false;
    }
}
