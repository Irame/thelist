package de.irame.thelist.Data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

/**
 * Created by Felix on 23.06.2014.
 */
public class OptionDAOImpl implements OptionDAO {
    private static final OptionDAOImpl ourInstance = new OptionDAOImpl(DBWrapper.getInstance().getConnection());

    private PreparedStatement storeStatement;
    private PreparedStatement findStatement;


    private OptionDAOImpl(Connection connection) {
        try {
            storeStatement = connection.prepareStatement("INSERT OR REPLACE INTO options (opt_key, opt_value) VALUES (?1, ?2);");
            findStatement = connection.prepareStatement("SELECT opt_value FROM options WHERE opt_key = ?1;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static OptionDAO getInstance() {
        return ourInstance;
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return getValue(key, defaultValue, Boolean::parseBoolean);
    }

    @Override
    public String getString(String key, String defaultValue) {
        return getValue(key, defaultValue, String::toString);
    }

    @Override
    public int getInt(String key, int defaultValue) {
        return getValue(key, defaultValue, Integer::parseInt);
    }

    @Override
    public void storeBoolean(String key, boolean value) {
        storeValue(key, String.valueOf(value));
    }

    @Override
    public void storeString(String key, String value) {
        storeValue(key, value);
    }

    @Override
    public void storeInt(String key, int value) {
        storeValue(key, String.valueOf(value));
    }

    // stores the value with key in the db
    private void storeValue(String key, String value) {
        try {
            storeStatement.setString(1, key);
            storeStatement.setString(2, value);
            storeStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // gets an value for a key with an specific type
    private <T> T getValue(String key, T defaultValue, Function<String, T> parseFunction) {
        T returnValue = defaultValue;
        try {
            findStatement.setString(1, key);
            ResultSet resultSet = findStatement.executeQuery();
            if (resultSet.next()) {
                returnValue = parseFunction.apply(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnValue;
    }
}
