package de.irame.thelist.Data;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Felix on 22.06.2014.
 */

/**
 * Data access object for the Champion class
 *
 * @see de.irame.thelist.Data.Champion
 */
public interface ChampionDAO {

    /**
     * Find one champion by id.
     *
     * @param id ID of the champ.
     * @return Champion matching the ID
     * @see de.irame.thelist.Data.Champion
     */
    public Champion findByID(int id);

    /**
     * Find all champions.
     * @return HashMap with all Champions
     * @see de.irame.thelist.Data.Champion
     */
    public Map<Integer, Champion> findAll();

    /**
     * Find all champions and put them into a given map
     *
     * @param champions map to put the champs into
     */
    public void findAll(Map<Integer, Champion> champions);

    /**
     * Updates the status of a champion (lock status and lockedSince date)
     * @see de.irame.thelist.Data.Champion
     */
    public void updateChampion(Champion champion);

    /**
     * Updates the status of all champions of a Collection (lock status and lockedSince date)
     * @see de.irame.thelist.Data.Champion
     */
    public void updateChampions(Collection<Champion> champions);
}
