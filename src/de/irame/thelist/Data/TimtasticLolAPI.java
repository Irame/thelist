package de.irame.thelist.Data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Felix on 12.06.2014.
 */

/**
 * Class to access the TimtasticLolAPI
 */
public class TimtasticLolAPI {
    private int gameId;
    private LinkedList<Integer> champsInGame;

    /**
     * Scans for a summoner in a region
     *
     * @param sumName Summoner name
     * @param region  region code
     * @throws UnirestException
     * @throws TimtasticLolAPIException
     */
    public TimtasticLolAPI(String sumName, String region) throws UnirestException, TimtasticLolAPIException {
        HttpResponse<JsonNode> request = Unirest.get("https://community-league-of-legends.p.mashape.com/api/v1.0/" + region + "/summoner/retrieveInProgressSpectatorGameInfo/" + sumName)
                .header("X-Mashape-Authorization", "zbsADjd5aDEYgezeBpKNlowGfkq3PJML")
                .asJson();

        if (request.getCode() != 200) {
            throw new TimtasticLolAPIException("Response status: " + request.getCode());
        }

        JSONObject bodyObject = request.getBody().getObject();

        if (bodyObject.has("error")) {
            throw new TimtasticLolAPIException(bodyObject.getString("error"));
        }

        gameId = bodyObject.getJSONObject("playerCredentials").getInt("gameId");

        champsInGame = new LinkedList<>();
        JSONArray pickList = bodyObject.getJSONObject("game").getJSONObject("playerChampionSelections").getJSONArray("array");
        for (int i = 0; i < pickList.length(); i++) {
            champsInGame.add(pickList.getJSONObject(i).getInt("championId"));
        }
    }

    /**
     * Gets the game ID from the game found in the constructor
     *
     * @return the game ID
     */
    public int getGameId() {
        return gameId;
    }

    /**
     * Gets an list of champion IDs of the game found in the constructor
     *
     * @return list of champion IDs
     */
    public LinkedList<Integer> getChampsInGame() {
        return champsInGame;
    }

    public class TimtasticLolAPIException extends Exception {
        public TimtasticLolAPIException(String msg) {
            super(msg);
        }
    }
}
