package de.irame.thelist.Data;

import de.irame.thelist.Debug;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Felix on 22.06.2014.
 */
public class ChampionDAOImpl implements ChampionDAO {
    private static final ChampionDAO ourInstance = new ChampionDAOImpl(DBWrapper.getInstance().getConnection());

    private PreparedStatement findByIdStatement;
    private PreparedStatement findAllStatement;

    private PreparedStatement updateChampionStatement;

    private ChampionDAOImpl(Connection connection) {
        try {
            findByIdStatement = connection.prepareStatement("SELECT * FROM champion_lock WHERE champ_ID = ?1;");
            findAllStatement = connection.prepareStatement("SELECT * FROM champion_lock;");

            updateChampionStatement = connection.prepareStatement("INSERT OR REPLACE INTO champion_lock (champ_ID, champ_is_locked, champ_locked_since) VALUES (?1, ?2, ?3);");
        } catch (SQLException e) {
            Debug.out(System.err::println, "ChampionDAOImpl init failed.\n");
        }
    }

    public static ChampionDAO getInstance() {
        return ourInstance;
    }

    @Override
    public Champion findByID(int id) {
        Champion champion;
        boolean locked = false;
        Date lockedSince = null;

        // fetching data from Database
        try {
            findByIdStatement.setInt(1, id);
            ResultSet resultSet = findByIdStatement.executeQuery();
            if (resultSet.next()) {
                locked = resultSet.getBoolean(2);
                lockedSince = resultSet.getDate(3);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // fetching data from RiotAPI
        champion = RiotAPI.getChampById(id);

        // merging data
        if (champion != null) {
            champion.setLocked(locked);
            champion.setLockedSince(lockedSince);
        }

        return champion;
    }

    @Override
    public Map<Integer, Champion> findAll() {
        HashMap<Integer, Champion> champions = new HashMap<>();
        findAll(champions);
        return champions;
    }

    @Override
    public void findAll(Map<Integer, Champion> champions) {
        ResultSet championsFromDB;

        try {
            Debug.startTimer("fetching data from Database");
            // fetching data from Database
            championsFromDB = findAllStatement.executeQuery();
            Debug.timerDiff("fetching data from Database");

            Debug.startTimer("fetching data from RiotAPI");
            // fetching data from RiotAPI
            RiotAPI.getAllChamps(champions);
            Debug.timerDiff("fetching data from RiotAPI");

            // merging data
            if (champions != null && championsFromDB != null) {
                while (championsFromDB.next()) {
                    Champion curChamp = champions.get(championsFromDB.getInt(1));
                    if (curChamp != null) {
                        curChamp.setLocked(championsFromDB.getBoolean(2));
                        curChamp.setLockedSince(championsFromDB.getDate(3));
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateChampion(Champion champion) {
        if (champion == null) return;
        try {
            long lockedSince;

            if (champion.getLockedSince() == null)
                lockedSince = 0;
            else
                lockedSince = champion.getLockedSince().getTime();

            updateChampionStatement.setInt(1, champion.getId());
            updateChampionStatement.setBoolean(2, champion.isLocked());
            updateChampionStatement.setLong(3, lockedSince);
            updateChampionStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateChampions(Collection<Champion> champions) {
        if (champions.size() < 1) return;
        long lockedSince;
        boolean oldAutoCommit;
        Connection connection;

        try {
            connection = updateChampionStatement.getConnection();
            oldAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(false);

            for (Champion champion : champions) {
                if (champion.getLockedSince() == null)
                    lockedSince = 0;
                else
                    lockedSince = champion.getLockedSince().getTime();

                updateChampionStatement.setInt(1, champion.getId());
                updateChampionStatement.setBoolean(2, champion.isLocked());
                updateChampionStatement.setLong(3, lockedSince);


                updateChampionStatement.addBatch();
            }
            updateChampionStatement.executeBatch();

            connection.commit();
            connection.setAutoCommit(oldAutoCommit);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
