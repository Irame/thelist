package de.irame.thelist.Data;

import de.irame.thelist.Debug;
import de.irame.thelist.Util;
import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.io.*;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * Created by Felix on 23.06.2014.
 */

/**
 * Singleton for caching files
 */
public class Cacher {
    // Singleton signature START
    private static Cacher ourInstance = new Cacher();
    private ExecutorService executorService;

    private Cacher() {

    }

    public static Cacher getInstance() {
        return ourInstance;
    }
    // Singleton signature END

    /**
     * Shut down the executor service
     *
     * @see java.util.concurrent.ExecutorService
     */
    public void shutdown() {
        executorService.shutdown();
    }

    /**
     * Initialise the executor service or restart it
     *
     * @see java.util.concurrent.ExecutorService
     */
    public void boot() {
        if (executorService != null) {
            executorService.shutdown();
        }
        executorService = Executors.newFixedThreadPool(25);
    }

    /**
     * Download and saves a file and call an callback method with an FileInputStream of this file.
     *
     * @param sourceUrl URL where the file should be downloaded from.
     * @param destPath  File path where the file should be saved to.
     * @param callback  Callback method which should be called with the FileInputStream of the downloaded file (it is possible that this function isn't called at all).
     * @see java.io.FileInputStream
     */
    public void cacheFile(String sourceUrl, String destPath, Consumer<FileInputStream> callback) {
        if (executorService == null) return;
        try {
            File file = new File(destPath);
            File dir = file.getParentFile();
            if (dir.exists() || dir.mkdirs()) {
                if (file.exists() && file.length() > 0) {
                    FileInputStream fis = new FileInputStream(file);
                    Platform.runLater(() -> callback.accept(fis));
                } else {
                    executorService.execute(new DownloadableFile(
                            new URL(sourceUrl),
                            new FileOutputStream(file),
                            callback,
                            new FileInputStream(file)));
                }
            } else {
                Util.setStatusMsgLabelText("Could not create cache directory!", Color.RED);
            }
        } catch (MalformedURLException | FileNotFoundException e) {
            Util.setStatusMsgLabelText("Could not cache file: " + sourceUrl, Color.RED);
            e.printStackTrace();
        }
    }

    /**
     * Downloadable File
     *
     * @see java.lang.Runnable
     */
    private class DownloadableFile implements Runnable {
        private URL fileSource;
        private FileOutputStream fileDest;
        private Consumer<FileInputStream> callback;
        private FileInputStream fis;

        /**
         * Creates an Downloadable File.
         *
         * @param fileSource URL where the file should be downloaded from.
         * @param fileDest   FileOutputStream which represent the output file.
         * @param callback   Callback method which should be called with the FileInputStream passed.
         * @param fis        FileInputStream the callback method is called with.
         */
        public DownloadableFile(URL fileSource, FileOutputStream fileDest, Consumer<FileInputStream> callback, FileInputStream fis) {
            this.fileSource = fileSource;
            this.fileDest = fileDest;
            this.callback = callback;
            this.fis = fis;
        }

        @Override
        public void run() {
            try {
                Debug.println("Start Downloading " + fileSource);
                ReadableByteChannel rbc = Channels.newChannel(fileSource.openStream());
                fileDest.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            } catch (ConnectException connectException) {
                Debug.out(System.err::println, connectException.toString());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                Debug.println("finished Downloading " + fileSource);
                Platform.runLater(() -> callback.accept(fis));
            }
        }
    }
}
