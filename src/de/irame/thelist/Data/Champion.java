package de.irame.thelist.Data;

import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Felix on 10.06.2014.
 */

/**
 * Class which represents a champion
 */
public class Champion {
    private final int id;
    private final String name;
    private final String key;
    private boolean locked;
    private Date lockedSince;
    private String imageName;
    private String version;
    private LinkedList<String> tags;

    public Champion(int id, String name, String key, String imageName, String version, boolean locked) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.locked = locked;
        this.imageName = imageName;
        this.version = version;
        tags = new LinkedList<>();
    }

    public Champion(int id, String name, String key, String imageName, String version) {
        this(id, name, key, imageName, version, false);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String getImageName() {
        return imageName;
    }

    public String getVersion() {
        return version;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Date getLockedSince() {
        return lockedSince;
    }

    public void setLockedSince(Date lockedSince) {
        this.lockedSince = lockedSince;
    }

    public void addTag(String tag) {
        tags.add(tag);
    }

    /**
     * Checks if an string matches with one of the champion tags.
     *
     * @param testString The tested String
     * @return true if the string matches one of the tags, false if not
     */
    public boolean matchWithTags(String testString) {
        for (String tag : tags) {
            return tag.equals(testString);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Champion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", locked=" + locked +
                '}';
    }
}
