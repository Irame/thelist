package de.irame.thelist.Scanner;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.irame.thelist.Data.Champion;
import de.irame.thelist.Data.RiotAPI;
import de.irame.thelist.Data.TimtasticLolAPI;
import de.irame.thelist.Debug;
import de.irame.thelist.Util;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Felix on 12.06.2014.
 */
public class ScannerWorker implements Runnable {
    ReadOnlyObjectProperty<String> region;
    StringProperty sumName;
    ConcurrentHashMap<Integer, Champion> champMap;
    Runnable updateFunction;
    int lastGameId;
    private boolean isRunning = true;


    public ScannerWorker(ReadOnlyObjectProperty<String> region, StringProperty sumName, ConcurrentHashMap<Integer, Champion> champMap, Runnable updateFunction) {
        this.region = region;
        this.sumName = sumName;
        this.champMap = champMap;
        this.updateFunction = updateFunction;
    }

    @Override
    public void run() {
        isRunning = true;
        TimtasticLolAPI timtasticLolAPI;

        while (isRunning) {
            if (!sumName.getValue().equals("") && region.getValue() != null) {
                Debug.println("Requesting information about:");
                Debug.println("Name:\t" + sumName.getValue());
                Debug.println("Region:\t" + region.getValue());
                if (RiotAPI.doesSummonerExist(sumName.getValue(), region.getValue())) {
                    try {
                        timtasticLolAPI = new TimtasticLolAPI(sumName.getValue(), region.getValue());
                        int curGameId = timtasticLolAPI.getGameId();
                        Util.setStatusMsgLabelText("Game found with ID = " + curGameId + " (last scanned ID = " + lastGameId + ")");
                        if (curGameId != lastGameId) {
                            LinkedList<Integer> lockedChamps = timtasticLolAPI.getChampsInGame();
                            Champion curChamp;
                            for (Integer lockedChamp : lockedChamps) {
                                curChamp = champMap.get(lockedChamp);
                                if (!curChamp.isLocked())
                                    curChamp.setLockedSince(new Date());
                                curChamp.setLocked(true);
                            }
                            Platform.runLater(updateFunction);
                            lastGameId = curGameId;
                        }
                    } catch (UnirestException | TimtasticLolAPI.TimtasticLolAPIException e) {
                        Util.setStatusMsgLabelText("Summoner \"" + sumName.getValue() + "\" is not in a game.", Color.RED);
                    }
                } else {
                    Util.setStatusMsgLabelText("Summoner \"" + sumName.getValue() + "\" does not exist.", Color.RED);
                }
            } else {
                Util.setStatusMsgLabelText("Missing region or summoner name", Color.RED);
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                Debug.out(System.err::println, getClass().getName() + " woke up");
            }

        }
    }

    public void stop() {
        isRunning = false;
    }

    public void start() {
        isRunning = true;
    }
}
