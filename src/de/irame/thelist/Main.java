package de.irame.thelist;

import com.mashape.unirest.http.Unirest;
import de.irame.thelist.GUI.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Felix on 10.06.2014.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Debug.startTimer("Loading GUI");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GUI/MainWindow.fxml"));
        Debug.startTimer("fxmlLoader.load()");
        Parent root = fxmlLoader.load();
        Debug.timerDiff("fxmlLoader.load()");
        primaryStage.setTitle("TheList");
        Scene scene = new Scene(root, 600, 400);
        scene.getStylesheets().add("de/irame/thelist/GUI/MainWindowStyle.css");
        primaryStage.setScene(scene);
        Debug.startTimer("primaryStage.show()");
        primaryStage.show();
        Debug.timerDiff("primaryStage.show()");
        Debug.timerDiff("Loading GUI");


        primaryStage.setOnCloseRequest(windowEvent -> {
            ((MainController) fxmlLoader.getController()).shutdown();
            try {
                Unirest.shutdown();
            } catch (IOException e) {
                Debug.println("Failed to shut down Unirest");
            }
        });


    }
}
