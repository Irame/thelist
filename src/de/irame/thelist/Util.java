package de.irame.thelist;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Felix on 12.06.2014.
 */
public abstract class Util {
    public static final String CACHE_DIR = System.getProperty("java.io.tmpdir") + "TheListCache/";
    public static final String IMAGE_CACHE_DIR = CACHE_DIR + "%s/img/champion/%s";
    public static final String IMAGE_URL_PATTERN = "http://ddragon.leagueoflegends.com/cdn/%s/img/champion/%s";

    private static Label statusMsgLabel = null;

    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<>(c);
        java.util.Collections.sort(list);
        return list;
    }

    public static String getImageUrl(String imageName, String version) {
        return String.format(IMAGE_URL_PATTERN, version, imageName);
    }

    public static void setStatusMsgLabel(Label label) {
        statusMsgLabel = label;
    }

    public static void setStatusMsgLabelText(String text) {
        setStatusMsgLabelText(text, Color.BLACK);
    }

    public static void setStatusMsgLabelText(String text, Color color) {
        if (statusMsgLabel != null) {
            Debug.println("StausMsg: " + text);
            Platform.runLater(() -> {
                statusMsgLabel.setText(text);
                statusMsgLabel.setTextFill(color);
            });
        }
    }
}
