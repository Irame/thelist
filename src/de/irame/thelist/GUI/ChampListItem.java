package de.irame.thelist.GUI;

import de.irame.thelist.Data.Cacher;
import de.irame.thelist.Data.Champion;
import de.irame.thelist.Util;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;


/**
 * Created by Felix on 10.06.2014.
 * <p>
 * Class to represent a "List Item" implemented as a VBox for the champ list implemented as a FlowPane.
 */
public class ChampListItem extends VBox {
    //Tooltip Text
    private static String tooltipString = "%s\nLocked since: %s";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    // champion to represent
    public final Champion champion;

    // name and image property's
    private ObjectProperty<Image> champImage;
    private StringProperty champName;

    // visual elements
    private Label champNameLabel;
    private ProgressIndicator progressIndicator;
    private ImageView champImageView;
    private InnerShadow borderGlowSelected;
    private InnerShadow borderGlowHover;
    private InnerShadow borderGlowHoverAndSelected;
    private InnerShadow borderGlowMouseDown;
    private Tooltip tooltip;

    //element status
    private boolean selected;
    private boolean hover;
    private boolean mouseDown;
    private boolean shown = true;

    /**
     * Creates a ChampListItem represents an Champion
     *
     * @param champion Champion to represent
     */
    public ChampListItem(Champion champion) {
        // set the userData to champ name to be able to compare them
        this.setUserData(champion.getName());

        this.champion = champion;

        // creating champ name label
        champNameLabel = new Label(champion.getName());
        this.champName = champNameLabel.textProperty();

        // creating champ image
        champImageView = new ImageView();
        this.champImage = champImageView.imageProperty();

        // cache champ image and call setChampImage(FileInputStream)
        Cacher.getInstance().cacheFile(
                String.format(Util.IMAGE_URL_PATTERN, champion.getVersion(), champion.getImageName()),
                String.format(Util.IMAGE_CACHE_DIR, champion.getVersion(), champion.getImageName()),
                this::setChampImage
        );

        // design this item (VBox)
        this.setAlignment(Pos.TOP_CENTER);
        this.setHeight(90);
        this.setWidth(70);

        // design the champ imageView
        champImageView.setFitWidth(70);
        champImageView.setFitHeight(70);
        champImageView.setCache(true);      // cache the image so it's independent from the file on disk

        // design the champ name label
        champNameLabel.setMaxWidth(70);
        champNameLabel.setAlignment(Pos.CENTER);

        // creating the ProgressIndicator which is displayed while the champ image is loading
        progressIndicator = new ProgressIndicator();
        progressIndicator.setPrefSize(50, 50);
        VBox.setMargin(progressIndicator, new Insets(10));

        // adding the progressIndicator and the name label to the VBox
        this.getChildren().addAll(progressIndicator, champNameLabel);

        //creating visual effects for the image
        borderGlowSelected = createBorderGlow(Color.GOLD, 50, 50, 0.5);
        borderGlowHover = createBorderGlow(Color.GREEN, 50, 50, 0.5);
        borderGlowHoverAndSelected = createBorderGlow(Color.BLUE, 50, 50, 0.5);
        borderGlowMouseDown = createBorderGlow(Color.RED, 50, 50, 0.5);

        // add mouse listeners
        this.hoverProperty().addListener((observableValue, booleanOld, booleanNew) -> {
            hover = booleanNew;
            updateStyle();
        });

        this.setOnMouseClicked(mouseEvent -> {
            selected = !selected;
            updateStyle();
        });

        this.setOnMousePressed(mouseEvent -> {
            mouseDown = true;
            updateStyle();
        });

        this.setOnMouseReleased(mouseEvent -> {
            mouseDown = false;
            updateStyle();
        });
    }

    /**
     * Set the champ image and replace the progressIndicator with it if it's still there
     *
     * @param imageUrl url to the image
     */
    public void setChampImage(String imageUrl) {
        this.champImage.setValue(new Image(imageUrl, true));
        if (progressIndicator != null) {
            ObservableList<Node> children = this.getChildren();
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i) == progressIndicator) {
                    children.set(i, champImageView);
                }
            }
            progressIndicator = null;
        }
    }

    /**
     * Set the champ image and replace the progressIndicator with it if it's still there
     *
     * @param fis FileInputStream of an image
     */
    public void setChampImage(FileInputStream fis) {
        this.champImage.setValue(new Image(fis));
        if (progressIndicator != null) {
            ObservableList<Node> children = this.getChildren();
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i) == progressIndicator) {
                    children.set(i, champImageView);
                }
            }
            progressIndicator = null;
        }
    }

    public String getChampName() {
        return champName.get();
    }

    public void setChampName(String champName) {
        this.champName.setValue(champName);
    }

    public StringProperty champNameProperty() {
        return champName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        updateStyle();
    }

    /**
     * Checks if the name or key contains the given string (key and name in lower case)
     *
     * @param searchString String to look for
     * @return true if the name or key contains the given string, false otherwise
     */
    public boolean matchWithSearch(String searchString) {
        return champion.getName().toLowerCase().contains(searchString) ||
                champion.getKey().toLowerCase().contains(searchString) ||
                champion.matchWithTags(searchString);
    }

    /**
     * updates the style of image and text (border glow for image and bold for text)
     */
    private void updateStyle() {
        if (mouseDown) {
            champImageView.setEffect(borderGlowMouseDown);
            champNameLabel.setStyle("-fx-font-weight: bold;");
        } else if (hover && selected) {
            champImageView.setEffect(borderGlowHoverAndSelected);
            champNameLabel.setStyle("-fx-font-weight: bold;");
        } else if (hover) {
            champImageView.setEffect(borderGlowHover);
            champNameLabel.setStyle("-fx-font-weight: bold;");
        } else if (selected) {
            champImageView.setEffect(borderGlowSelected);
            champNameLabel.setStyle("-fx-font-weight: bold;");
        } else {
            champImageView.setEffect(null);
            champNameLabel.setStyle("");
        }
    }

    /**
     * Creates an border glow effect with the given stats
     *
     * @param color  Color of the border glow
     * @param width  Width of the border glow
     * @param height Height of the border glow
     * @param choke  Choke  of the border glow (percentage of the glow effect with is totally filled with the given color
     * @return The resulting borderGlow as an InnerShadow
     * @see javafx.scene.effect.InnerShadow
     */
    private InnerShadow createBorderGlow(Color color, double width, double height, double choke) {
        InnerShadow borderGlow = new InnerShadow();
        borderGlow.setColor(color);
        borderGlow.setWidth(width);
        borderGlow.setHeight(height);
        borderGlow.setChoke(choke);

        return borderGlow;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }

    public boolean isLocked() {
        return champion.isLocked();
    }

    public boolean isFree() {
        return !champion.isLocked();
    }

    public void updateTooltip() {
        if (tooltip == null) {
            tooltip = new Tooltip();
            tooltip.setOnShown(windowEvent -> tooltip.setFont(Font.font(14)));
            Tooltip.install(this, tooltip);
        }

        if (champion.isLocked() && champion.getLockedSince() != null) {
            tooltip.setText(String.format(tooltipString, champion.getName(), dateFormat.format(champion.getLockedSince())));
            //Debug.out(System.out::println, tooltip.getText());
        } else {
            tooltip.setText(champion.getName());
        }
    }
}
