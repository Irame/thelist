package de.irame.thelist.GUI;

import javafx.scene.Node;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Created by Felix on 11.06.2014.
 */
public class ChampListItemComparator implements Comparator<Node> {

    @Override
    public int compare(Node o1, Node o2) {
        return ((String) o1.getUserData()).compareTo((String) o2.getUserData());
    }

    @Override
    public Comparator<Node> reversed() {
        return null;
    }

    @Override
    public Comparator<Node> thenComparing(Comparator<? super Node> other) {
        return null;
    }

    @Override
    public <U> Comparator<Node> thenComparing(Function<? super Node, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<Node> thenComparing(Function<? super Node, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Node> thenComparingInt(ToIntFunction<? super Node> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Node> thenComparingLong(ToLongFunction<? super Node> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Node> thenComparingDouble(ToDoubleFunction<? super Node> keyExtractor) {
        return null;
    }
}
