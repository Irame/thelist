package de.irame.thelist.GUI;

import de.irame.thelist.Data.*;
import de.irame.thelist.Debug;
import de.irame.thelist.Scanner.ScannerWorker;
import de.irame.thelist.Util;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Created by Felix on 10.06.2014.
 * <p>
 * This Class in the man Controller for this app and holds the most user input reaction logic and initializes the GUI
 */
public class MainController implements Initializable {
    // Keys to save GUI option data
    private final String OPT_KEY_AUTO_SAVE = "autoSave";
    private final String OPT_KEY_REGION = "region";
    private final String OPT_KEY_SUMMONER_NAME = "summonerName";

    // special search requests
    private final String SEARCH_RANDOM = "random";

    //GUI ELEMENTS
    public GridPane rootView_main;

    public TextField search_main_textField;

    public ScrollPane freeChamps_main_scrollPane;
    public FlowPane freeChamps_main_flowPane;
    public ScrollPane lockedChamps_main_scrollPane;
    public FlowPane lockedChamps_main_flowPane;
    public CheckBox autoSave_main_checkBox;

    public ComboBox<String> region_main_comboBox;
    public TextField sumName_main_TextField;
    public Button record_main_button;
    public Label statusMsg_main_label;

    // Scanner Thread declaration
    private ScannerWorker scannerWorker;
    private Thread scannerThread = null;

    // Champ List declaration
    private ChampListItemComparator champListItemComparator;
    private CopyOnWriteArrayList<ChampListItem> champListItems;
    private ConcurrentHashMap<Integer, Champion> champions;

    // GUI Options
    private boolean recording = false;
    private boolean autoSave = false;

    // Data Access Objects for Champions and Options
    private ChampionDAO championDAO;
    private OptionDAO optionsDAO;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // sets the status massage label in the Util class so u can write from everywhere to this textfield for error and information messages
        Util.setStatusMsgLabel(statusMsg_main_label);

        // init DAOs
        championDAO = ChampionDAOImpl.getInstance();
        optionsDAO = OptionDAOImpl.getInstance();

        // init champ lists
        champListItemComparator = new ChampListItemComparator();
        champListItems = new CopyOnWriteArrayList<>();
        champions = new ConcurrentHashMap<>();

        // init region combo bos items
        region_main_comboBox.getItems().addAll(
                "NA", "EUW", "EUNE", "BR", "TR", "RU", "LAN", "LAS", "OCE"
        );

        // add Esc unselect
        rootView_main.setOnKeyReleased((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE)
                unselectAll();
        });

        // filter champs so just matching ones are shown if the text in the search field changes
        search_main_textField.textProperty().addListener((observableValue, stringOld, stringNew) -> {
            String searchInput = stringNew.toLowerCase();
            if (searchInput.equals(SEARCH_RANDOM)) {
                List<ChampListItem> freeChamps = champListItems.stream().filter(ChampListItem::isFree).collect(Collectors.toList());
                int champToShow = (int) (Math.random() * freeChamps.size());
                for (int i = 0; i < freeChamps.size(); i++) {
                    freeChamps.get(i).setShown(champToShow == i);
                }
            } else {
                for (ChampListItem champListItem : champListItems) {
                    champListItem.setShown(champListItem.matchWithSearch(searchInput));
                }
            }
            updateChampLists(false);
        });

        // adds the ability to press enter in the search field and if there is just one champ shown it will be selected
        search_main_textField.setOnAction(actionEvent -> {
            List<ChampListItem> shownChaps = champListItems.stream().filter(ChampListItem::isShown).collect(Collectors.toList());
            if (shownChaps.size() == 1)
                shownChaps.get(0).setSelected(true);
        });

        // init GUI Options
        sumName_main_TextField.setText(optionsDAO.getString(OPT_KEY_SUMMONER_NAME, ""));
        region_main_comboBox.getSelectionModel().select(optionsDAO.getString(OPT_KEY_REGION, null));
        autoSave = optionsDAO.getBoolean(OPT_KEY_AUTO_SAVE, false);
        autoSave_main_checkBox.setSelected(autoSave);

        //adding listener for saving GUI Options
        sumName_main_TextField.textProperty().addListener((observableValue, stringOld, stringNew) -> optionsDAO.storeString(OPT_KEY_SUMMONER_NAME, stringNew));
        region_main_comboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, stringOld, stringNew) -> optionsDAO.storeString(OPT_KEY_REGION, stringNew));

        // init scanner
        scannerWorker = new ScannerWorker(region_main_comboBox.getSelectionModel().selectedItemProperty(), sumName_main_TextField.textProperty(), champions, this::updateChampLists);

        // start loading champs in an new thread
        new Thread(this::loadChamps).start();
    }

    // loading champs (usually called by another thread)
    private void loadChamps() {
        Debug.startTimer("championDAO.findAll()");
        championDAO.findAll(champions);
        Debug.timerDiff("championDAO.findAll()");


        //Platform.runLater(() -> {
        Debug.startTimer("loadChampImages");
        Cacher.getInstance().boot();
        for (Champion champion : champions.values()) {
            ChampListItem champListItem;
            champListItem = new ChampListItem(champion);
            champListItems.add(champListItem);
        }
        Cacher.getInstance().shutdown();
        Debug.timerDiff("loadChampImages");
        Platform.runLater(this::updateChampLists);
        //});
    }

    /**
     * Fires if the record button was clicked.
     * Starts/stops the scanning thread
     *
     * @param actionEvent
     */
    public void recordButtonClicked(ActionEvent actionEvent) {
        if (recording) {
            scannerWorker.stop();
            record_main_button.setText("Start");
        } else {
            if (scannerThread == null || !scannerThread.isAlive())
                (scannerThread = new Thread(scannerWorker)).start();
            scannerWorker.start();
            record_main_button.setText("Stop");
        }
        recording = !recording;
    }

    /**
     * Fires if the lock button was clicked.
     * Locks all champs who are selected.
     *
     * @param actionEvent
     */
    public void lockButtonClicked(ActionEvent actionEvent) {
        champListItems.stream().filter(ChampListItem::isSelected)
                .forEach(champListItem -> {
                    if (!champListItem.champion.isLocked())
                        champListItem.champion.setLockedSince(new Date());
                    champListItem.champion.setLocked(true);
                    champListItem.updateTooltip();
                });
        updateChampLists();
    }

    /**
     * Fires if the free button was clicked.
     * Unlocks all champs who are selected.
     *
     * @param actionEvent
     */
    public void freeButtonClicked(ActionEvent actionEvent) {
        champListItems.stream().filter(ChampListItem::isSelected)
                .forEach(champListItem -> champListItem.champion.setLocked(false));
        updateChampLists();
    }

    /**
     * Fires if the free all button was clicked.
     * Unlocks all champs.
     *
     * @param actionEvent
     */
    public void freeAllButtonClicked(ActionEvent actionEvent) {
        champions.values().stream().forEach(champion -> champion.setLocked(false));
        updateChampLists();
    }

    /**
     * Fires if the save button was clicked.
     * Saves the current Champ status to db.
     *
     * @param actionEvent
     */
    public void saveButtonClicked(ActionEvent actionEvent) {
        saveChampsStatus();
    }

    /**
     * Fires if the clipboard button was clicked.
     * Puts an well Formatted champ list into the clipboard.
     *
     * @param actionEvent
     */
    public void clipboardButtonClicked(ActionEvent actionEvent) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringBuilder builder = new StringBuilder();
        HashMap<Character, LinkedList<String>> lockedChamps = new HashMap<>();

        for (Champion champion : champions.values()) {
            if (champion.isLocked()) {
                char curChar = champion.getName().charAt(0);
                LinkedList<String> curList;
                if ((curList = lockedChamps.get(curChar)) == null)
                    lockedChamps.put(curChar, curList = new LinkedList<>());
                curList.add(champion.getName());
            }
        }
        for (Character character : Util.asSortedList(lockedChamps.keySet())) {
            builder.append(character).append(": ");
            builder.append(String.join(", ", lockedChamps.get(character)));
            builder.append('\n');
        }

        StringSelection stringSelection = new StringSelection(builder.toString());
        clipboard.setContents(stringSelection, stringSelection);
    }

    // Removes the selection from all Champions
    private void unselectAll() {
        for (ChampListItem champListItem : champListItems) {
            if (champListItem.isSelected())
                champListItem.setSelected(false);
        }
    }

    //Updates the GUI champ lists and saves it to db if autoSave is true
    private void updateChampLists() {
        Debug.startTimer("updateChampLists()");
        updateChampLists(true);
        Debug.timerDiff("updateChampLists()");
    }

    //Updates the GUI champ lists and saves it to db if autoSave and save is true
    private void updateChampLists(boolean save) {
        freeChamps_main_flowPane.getChildren().clear();
        lockedChamps_main_flowPane.getChildren().clear();
        for (ChampListItem champListItem : champListItems) {
            if (champListItem.isShown()) {
                if (!champListItem.champion.isLocked()) {
                    freeChamps_main_flowPane.getChildren().add(champListItem);
                } else {
                    lockedChamps_main_flowPane.getChildren().add(champListItem);
                }
                champListItem.updateTooltip();
            }
            //champListItem.setSelected(false);
        }
        FXCollections.sort(freeChamps_main_flowPane.getChildren(), champListItemComparator);
        FXCollections.sort(lockedChamps_main_flowPane.getChildren(), champListItemComparator);

        if (save && autoSave) saveChampsStatus();
    }

    /**
     * Saves the Champion status to the db.
     */
    public void saveChampsStatus() {
        championDAO.updateChampions(champions.values());
    }

    /**
     * Fires if the autoSave checkbox was clicked.
     * Sets the autoSave boolean variable to the checkbox status
     *
     * @param actionEvent
     */
    public void autoSaveCheckBoxClicked(ActionEvent actionEvent) {
        autoSave = autoSave_main_checkBox.isSelected();
        optionsDAO.storeBoolean(OPT_KEY_AUTO_SAVE, autoSave);
        saveChampsStatus();
    }

    /**
     * Stops the scanner.
     */
    public void shutdown() {
        scannerWorker.stop();
        if (scannerThread != null && scannerThread.isAlive())
            scannerThread.interrupt();
    }
}
