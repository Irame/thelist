package de.irame.thelist;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;

/**
 * Created by Felix on 12.06.2014.
 */
public class Debug {
    public static final boolean DEBUG = true;
    public static final boolean TIMING = true;
    public static HashMap<String, Long> timeMeasures = new HashMap<>();
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    public static void out(Consumer<Object> action, Object o) {
        if (DEBUG)
            action.accept(o);
    }

    public static void println(Object o) {
        if (DEBUG)
            System.out.println("[" + dateFormat.format(new Date()) + "] " + o);
    }

    public static void startTimer(String key) {
        if (!TIMING) return;
        String printString = "TIMER START: " + key + " (Thread: " + Thread.currentThread().getName() + ")";
        println("TIMER START: " + key + " (Thread: " + Thread.currentThread().getName() + ")");
        //Logging.logger.log(Level.INFO, printString);
        timeMeasures.put(key, System.currentTimeMillis());
    }

    public static long timerDiff(String key) {
        if (!TIMING) return -1;
        long diff = System.currentTimeMillis() - timeMeasures.get(key);
        String printString = "TIMER DIFF: " + key + " - " + diff + " ms";
        println(printString);
        //Logging.logger.log(Level.INFO, printString);
        return diff;
    }
}
